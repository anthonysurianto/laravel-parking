<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/parkings');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('/manual-logout', 'App\Http\Controllers\ParkingsController@logout')->name('manual.logout');
    Route::get('/parkings', 'App\Http\Controllers\ParkingsController@index')->name('parkings');
    Route::get('/list-parkings', 'App\Http\Controllers\ParkingsController@listParking')->name('list.parkings');
    Route::get('/export-parkings', 'App\Http\Controllers\ParkingsController@export')->name('export.parkings');
    Route::post('/list-parkings-filter', 'App\Http\Controllers\ParkingsController@listParkingFilter')->name('list.parkings.filter');
    Route::post('/parkings/store', 'App\Http\Controllers\ParkingsController@inputParking')->name('parkings.store');
    Route::post('/parkings/out', 'App\Http\Controllers\ParkingsController@outParking')->name('parkings.out');
});
