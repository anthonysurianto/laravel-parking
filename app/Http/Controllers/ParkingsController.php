<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Parkings;
use Carbon\Carbon;
use App\Exports\ParkingsExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;


class ParkingsController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        return view('parkings.index');
    }

    public function logout()
    {
        Auth::guard('web')->logout();    
        return redirect('/login');
    }

    public function inputParking(Request $request)
    {
        $request->validate([
            'no_pol'=>'required'
        ]);
        $check = Parkings::where(
                [
                    ['no_pol',$request->get('no_pol')],
                    ['time_out',NULL]
                ]
                )->get();
        if(count($check) > 0 ){
            return redirect('/parkings')->with('error', 'Vehicle with same No Pol is not out');        
        }
        $parking = new Parkings([
            'unique_code' => date("Ymdhis"),
            'no_pol' => $request->get('no_pol')
        ]);
        $parking->save();
        return redirect('/parkings')->with('success', 'Unique Code :'.$parking->unique_code);
    }

    public function outParking(Request $request)
    {
        $Price_Hour = 3000; 
        // $contacts = Contact::all();
        $request->validate([
            'unique_code'=>'required'
        ]);
        $parking = Parkings::where(
                [
                    ['unique_code',$request->get('unique_code')],
                    ['time_out',NULL]
                ]
                )->first();
        
        if($parking != NULL ){
            $parking->time_out = Carbon::now()->setTimezone('Asia/Jakarta');
            $diffHours = $parking->time_out->diffInHours($parking->time_in) + 1;
            $parking->hours = $diffHours;
            $parking->price = $Price_Hour * $diffHours;
            $parking->save();
            return redirect('/parkings')->with('success', 'Total Parking Time :'.$parking->hours.' hrs. Parking Price : Rp.'.$parking->price);
        }
        return redirect('/parkings')->with('success', 'Unique Code not found !');
    }


    public function listParking()
    {
        if(Auth::user()->roles != 'admin'){
            return redirect('/parkings');
        }
        $parkings = Parkings::all();
        return view('parkings.list', compact('parkings'));
    }

    public function listParkingFilter(Request $request)
    {
        if(Auth::user()->roles != 'admin'){
            return redirect('/parkings');
        }
        $startDate = $request->get('start_date')." 00:00:00";
        $endDate = $request->get('end_date')." 23:59:59";
        $parkings = Parkings::whereBetween(
                'time_in',[$startDate,$endDate]
            )->get();
        return view('parkings.list', compact('parkings'));
    }

    public function export(){   
        if(Auth::user()->roles != 'admin'){
            return redirect('/parkings');
        } 
        return Excel::download(new ParkingsExport, 'parkings.xlsx');
    }


}
