<?php

namespace App\Exports;

use App\Models\Parkings;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ParkingsExport implements FromView
{
    public function view(): View
    {
        // return Parkings::all();
        // $parkings = Parkings::all();
        return view('parkings.export',[
            'parkings' => Parkings::all()
        ]);
    }
}