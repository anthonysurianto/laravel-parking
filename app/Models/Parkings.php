<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Parkings extends Model
{
    use HasFactory;

    protected $fillable = [
        'unique_code', 'no_pol', 'time_out', 'hours', 'price'
    ];
}
