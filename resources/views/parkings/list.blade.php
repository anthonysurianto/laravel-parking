@extends('base')
@section('main')
<div class="row">
<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
<div class="col-sm-12">
    <h1 class="display-3">Parkings</h1>    
    <div class="col-sm-4">
      <form action="{{ route('list.parkings.filter')}}" method="post">
          @csrf
          <div class="form-group">
              <label for="start_date">Start Date:</label>
              <input type="date" id="start_date" class="form-control" name="start_date" required/>
          </div>
          <div class="form-group">
              <label for="end_date">End Date:</label>
              <input type="date" id="end_date" class="form-control" name="end_date" required/>
          </div>
          <button class="btn btn-info" type="submit">Filter</button>
      </form>
    </div>
    <div class="col-md-12 col-sm-offset-10">
        <a style="margin: 19px;" href="{{ route('export.parkings')}}" class="btn btn-primary">Download Excel</a>
    </div>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Unique Code</td>
          <td>No Pol</td>
          <td>Time In</td>
          <td>Time Out</td>
          <td>Hours</td>
          <td>Price</td>
        </tr>
    </thead>
    <tbody>
        @foreach($parkings as $parking)
        <tr>
            <td>{{$parking->id}}</td>
            <td>{{$parking->unique_code}}</td>
            <td>{{$parking->no_pol}}</td>
            <td>{{$parking->time_in}}</td>
            <td>{{$parking->time_out}}</td>
            <td>{{$parking->hours}}</td>
            <td>{{$parking->price}}</td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>
@push('scripts')
    <script src="{{ asset('js/date.js') }}" defer></script>
@endpush
@endsection