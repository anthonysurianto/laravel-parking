@extends('base')
@section('main')
<div class="row">
    <div class="col-sm-12">
    @if(session()->get('success'))
        <div class="alert alert-success">
        {{ session()->get('success') }}  
        </div>
    @endif
    @if(session()->get('error'))
        <div class="alert alert-error">
        {{ session()->get('error') }}  
        </div>
    @endif
    </div>
    <div class="col-sm-12">
        <h1 class="display-3">Parkings In</h1>    
    </div>
    <div class="col-md-4 ">
        <form method="post" action="{{ route('parkings.store') }}">
            @csrf
            <div class="form-group">    
                <label for="no_pol">No Pol:</label>
                <input type="text" class="form-control" name="no_pol"/>
            </div>
            <button type="submit" class="btn btn-primary-outline">In</button>
        </form>
    </div>
    <div class="col-sm-12">
        <h1 class="display-3">Parkings Out</h1>    
    </div>
    <div class="col-md-4">
        <form method="post" action="{{ route('parkings.out') }}">
            @csrf
            <div class="form-group">    
                <label for="unique_code">Unique Code:</label>
                <input type="text" class="form-control" name="unique_code"/>
            </div>
            <button type="submit" class="btn btn-primary-outline">Out</button>
        </form>
    </div>
    <div>
</div>
@endsection