<table>
    <thead>
    <tr>
        <th>Id</th>
        <th>Unique Code</th>
        <th>No Pol</th>
        <th>Time In</th>
        <th>Time Out</th>
        <th>Hours</th>
        <th>Price</th>
    </tr>
    </thead>
    <tbody>
    @foreach($parkings as $parking)
        <tr>
            <td>{{$parking->id}}</td>
            <td>{{$parking->unique_code}}</td>
            <td>{{$parking->no_pol}}</td>
            <td>{{$parking->time_in}}</td>
            <td>{{$parking->time_out}}</td>
            <td>{{$parking->hours}}</td>
            <td>{{$parking->price}}</td>
        </tr>
        @endforeach
    </tbody>
</table>